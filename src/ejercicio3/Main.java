package ejercicio3;

import com.db4o.*;
import com.db4o.query.*;
import java.io.*;

public class Main {

  public static void main(String[] args) {
    new File("alumnos.db4o").delete();
    ObjectContainer bd =
            Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(),
            "alumnos.db4o");
    try {

      almacenarProyectos(bd);

      mostrarAlgunosAlumnos(bd);
    } catch (Exception e) {
    } finally {

      bd.close();
    }
  }

  public static void almacenarProyectos(ObjectContainer bd) {
    proyecto p1 = new proyecto("Robot con microcontroladores");
    alumno a1 = new alumno("Juan Gámez", 23, 8.75);
    p1.setAlumno(a1);
    bd.store(p1);
    System.out.println(p1.toString() + " Almacenado");

    proyecto p2 = new proyecto("Webmin sobre Ubuntu");
    alumno a2 = new alumno("Emilio Anaya", 24, 6.25);
    p2.setAlumno(a2);
    bd.store(p2);
    System.out.println(p2.toString() + " Almacenado");

    proyecto p3 = new proyecto("Joomla y Drupal");
    alumno a3 = new alumno("Angeles Blanco", 26, 7);
    p3.setAlumno(a3);
    bd.store(p3);
    System.out.println(p3.toString() + " Almacenado");

    proyecto p4 = new proyecto("Doctor Profiler");
    alumno a4 = new alumno("Luis Alberto García", 29, 9.5);
    p4.setAlumno(a4);
    bd.store(p4);
    System.out.println(p4.toString() + " Almacenado");
  }

  public static void mostrarResultado(ObjectSet res) {
    System.out.println("Recuperados " + res.size() + " Objetos");
    while (res.hasNext()) {

      System.out.println(res.next());
    }
  }

  public static void mostrarAlgunosAlumnos(ObjectContainer bd) {
    Query query = bd.query();
    query.constrain(alumno.class);
    query.descend("nombre").constrain("Fernando Gil").not();
    ObjectSet result = query.execute();
    mostrarResultado(result);
  }
}
