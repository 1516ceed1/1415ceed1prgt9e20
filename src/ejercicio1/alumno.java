package ejercicio1;

public class alumno {

  private String nombre;
  private int edad;
  private double nota;

  alumno() {
    nombre = null;
    edad = 0;
    nota = 0;
  }

  alumno(String nom, int e, double n) {

    nombre = nom;
    edad = e;
    nota = n;
  }

  public void setNombre(String n) {
    nombre = n;
  }

  @Override
  public String toString() {
    return this.nombre + " " + this.edad + " " + this.nota;
  }

  String getNombre() {
    return nombre;
  }
}
