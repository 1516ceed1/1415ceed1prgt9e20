package alumno;
import com.db4o.*;
import com.db4o.ext.DatabaseClosedException;
import com.db4o.ext.Db4oIOException;
import com.db4o.query.Constraint;
import com.db4o.query.Query;
import java.io.File;
import java.io.IOException;

public class Main {

  public static void borrarBD(String nombrefichero) 
  throws IOException
  {
    System.out.println("* Borrando BD "+nombrefichero);
    File f = new File(nombrefichero);
    f.delete();
  }

 
  public static ObjectContainer crearBD( String nombrefichero) {
    System.out.println("* Creando BD "+nombrefichero);
    return  Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(),
            nombrefichero);
  }

  public static void cerrarBD(ObjectContainer bd) throws Db4oIOException {
    boolean closed = bd.close();
  }

  
  public static void almacena(ObjectContainer bd) {
    System.out.println("* Grabando Alumnos:");
    alumno a1 = new alumno("Juan", 15, 4.5);
    bd.store(a1);
    System.out.println(a1.getNombre()+" Almacenado");
    alumno a2 = new alumno("Vicente", 50, 8.1);
    bd.store(a2);
    System.out.println(a2.getNombre()+" Almacenado");
    alumno a3 = new alumno("Paco", 25, 10.0);
    bd.store(a3);
    System.out.println(a3.getNombre()+" Almacenado");
  }
  
   public static void consulta1(ObjectContainer bd) 
          throws DatabaseClosedException {
    

    System.out.println("* Consulta 1 SODA: Alumnos");
    Query query = bd.query();
    Constraint constrain = query.constrain(alumno.class);
    ObjectSet res = query.execute();
    mostrarResultados(res);
  }

  

      public static void consulta6(ObjectContainer bd) 
          throws DatabaseClosedException {
    
    System.out.println("* Consulta 6 Soda: Alumno con edad menor 40"
            + " o nota no mayor que 9 ");
    Query query = bd.query();
    Constraint constrain = query.constrain(alumno.class);
    constrain = query.descend("edad").constrain(40).smaller();
    constrain = query.descend("nota").constrain(9).greater()
            .or(constrain).not();
    ObjectSet res = query.execute();
    mostrarResultados(res);
  }

  public static void mostrarResultados(ObjectSet res) {
    System.out.println("Objetos alumno recuperados: "+res.size());
    while (res.hasNext()) {
        System.out.println(res.next()); //toString
    }
  }
   
  public static void main(String args[]) throws IOException {
    
    ObjectContainer bd;
    String nombrefichero = "alumnossoda.db4o";
    
    borrarBD(nombrefichero);
    bd=crearBD(nombrefichero);
    almacena(bd);
    consulta1(bd);
    consulta6(bd);
    cerrarBD(bd);
  }
}
  
  
  

